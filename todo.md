# TODO

## Core language
- Del operator
- some libraries (io, math, ...)
- Assign should take an expression as lvalue
- native functions
- for loop
- while loop
- foreach

- curification
- pattern matching
- destructuring assignement
- varargs

- error reporting


## Parser
- Base of recursive descent parser


## Optimisation
- constant propagation
- partial evaluation
- terminal recursivity into loop
