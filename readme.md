# Language training

Training on computer language : parsers, compilers and interpreters


## Progression

### AST and semantics

AST est un arbre qui représente le code.

1. integer literal node and add node
2. more binary opérations: sub, mul, div, modulo, ...
3. unary operation: neg, sqr, sqrt, abs, floor, ceil, round, ...
4. boolean node with and, or and not opérations
5. string literal with concat
6. undefined literal ?
7. variables with frame and values
8. assign
9. if/then/else
10. functions/call function as first class citizen => functor values
11. sequence


12. return value
13. code fizzbuzz

14. array and index
15. tuple and index
16. dict and index

17. lazy logical operations (or, and)
18. lazy functions call (try to be configurable)
