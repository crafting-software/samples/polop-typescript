import { List } from "immutable";

import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class ArrayLiteral implements Expression {
  constructor(public readonly values: List<Expression>) {
  }

  public isConstant(): boolean {
    return true;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitArrayLiteral(this);
  }

  public changeValues(values: List<Expression>): ArrayLiteral {
    return new ArrayLiteral(values);
  }
}
