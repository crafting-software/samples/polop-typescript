import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class StringLiteral implements Expression {
  constructor(public readonly value: string) {
  }

  public isConstant(): boolean {
    return true;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitStringLiteral(this);
  }
}
