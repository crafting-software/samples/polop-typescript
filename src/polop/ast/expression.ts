import { ExpressionVisitor } from "polop/ast/expression_visitor";


export interface Expression {
  isConstant(): boolean;
  accept(visitor: ExpressionVisitor): any;
}
