import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class Index implements Expression {
  constructor(
    public readonly value: Expression,
    public readonly index: Expression) {
  }

  public isConstant(): boolean {
    return this.value.isConstant() && this.index.isConstant();
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitIndex(this);
  }

  public changeValue(value: Expression): Index {
    return new Index(value, this.index);
  }

  public changeIndex(index: Expression): Index {
    return new Index(this.value, index);
  }
}
