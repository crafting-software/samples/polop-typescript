import { ArrayLiteral } from "polop/ast/array_literal";
import { Assign } from "polop/ast/assign";
import { BinaryOperator } from "polop/ast/binary_operator";
import { BooleanLiteral } from "polop/ast/boolean_literal";
import { DictLiteral } from "polop/ast/dict_literal";
import { FunctionCall } from "polop/ast/function_call";
import { FunctionDefinition } from "polop/ast/function_definition";
import { IfThenElse } from "polop/ast/if_then_else";
import { Index } from "polop/ast/index";
import { NumberLiteral } from "polop/ast/number_literal";
import { Return } from "polop/ast/return";
import { Sequence } from "polop/ast/sequence";
import { StringLiteral } from "polop/ast/string_literal";
import { TupleLiteral } from "polop/ast/tuple_literal";
import { UnaryOperator } from "polop/ast/unary_operator";
import { UndefinedLiteral } from "polop/ast/undefined_literal";
import { VariableReference } from "polop/ast/variable";


export interface ExpressionVisitor {
  visitArrayLiteral(expression: ArrayLiteral): any;
  visitAssign(expression: Assign): any;
  visitBinaryOperator(expression: BinaryOperator): any;
  visitBooleanLiteral(expression: BooleanLiteral): any;
  visitDictLiteral(expression: DictLiteral): any;
  visitFunctionCall(expression: FunctionCall): any;
  visitFunctionDefinition(expression: FunctionDefinition): any;
  visitIfThenElse(expression: IfThenElse): any;
  visitIndex(expression: Index): any;
  visitNumberLiteral(expression: NumberLiteral): any;
  visitReturn(expression: Return): any;
  visitSequence(expression: Sequence): any;
  visitStringLiteral(expression: StringLiteral): any;
  visitTupleLiteral(expression: TupleLiteral): any;
  visitUnaryOperator(expression: UnaryOperator): any;
  visitUndefinedLiteral(expression: UndefinedLiteral): any;
  visitVariableReference(expression: VariableReference): any;
}
