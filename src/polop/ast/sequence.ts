import { List } from "immutable";

import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class Sequence implements Expression {
  constructor(
    public readonly children: List<Expression>) {
  }

  public isConstant(): boolean {
    return false;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitSequence(this);
  }

  public changeChildren(children: List<Expression>): Sequence {
    return new Sequence(children);
  }

}
