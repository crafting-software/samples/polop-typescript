import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class VariableReference implements Expression {
  constructor(public readonly name: string) {
  }

  public isConstant(): boolean {
    return false;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitVariableReference(this);
  }
}
