import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export enum BinaryOperators {
  ADDITION, // number
  AND, // boolean
  CONCAT, // string, tuple, array and dict
  EQUAL,
  FLOAT_DIVISION,  // number
  GREATER_OR_EQUAL,
  GREATER_THAN,
  INTEGER_DIVISION,
  LESS_OR_EQUAL,
  LESS_THAN,
  MODULO,  // number
  MULTIPLICATION,  // number
  NOT_EQUAL,
  OR, // boolean
  SUBSTRACTION,  // number
  XOR, // boolean
}


export class BinaryOperator implements Expression {
  constructor(
    public readonly operator: BinaryOperators,
    public readonly left: Expression,
    public readonly right: Expression) {
  }

  public isConstant(): boolean {
    return this.left.isConstant() && this.right.isConstant();
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitBinaryOperator(this);
  }

  public changeOperator(operator: BinaryOperators): BinaryOperator {
    return new BinaryOperator(operator, this.left, this.right);
  }

  public changeLeft(left: Expression): BinaryOperator {
    return new BinaryOperator(this.operator, left, this.right);
  }

  public changeRight(right: Expression): BinaryOperator {
    return new BinaryOperator(this.operator, this.left, right);
  }
}
