import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class UndefinedLiteral implements Expression {
  public isConstant(): boolean {
    return true;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitUndefinedLiteral(this);
  }
}
