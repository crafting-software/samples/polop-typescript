import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class Assign implements Expression {
  constructor(
    public readonly name: string,
    public readonly value: Expression) {
  }

  public isConstant(): boolean {
    return false;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitAssign(this);
  }

  public changeValue(value: Expression): Assign {
    return new Assign(this.name, value);
  }
}
