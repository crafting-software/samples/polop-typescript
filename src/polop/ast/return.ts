import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class Return implements Expression {
  constructor(
    public readonly value: Expression) {
  }

  public isConstant(): boolean {
    return this.value.isConstant();
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitReturn(this);
  }

  public changeValue(value: Expression): Return {
    return new Return(value);
  }
}
