import { Seq, Set } from "immutable";

import { ArrayLiteral } from "polop/ast/array_literal";
import { Assign } from "polop/ast/assign";
import { BinaryOperator } from "polop/ast/binary_operator";
import { BooleanLiteral } from "polop/ast/boolean_literal";
import { DictLiteral } from "polop/ast/dict_literal";
import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";
import { FunctionCall } from "polop/ast/function_call";
import { FunctionDefinition } from "polop/ast/function_definition";
import { IfThenElse } from "polop/ast/if_then_else";
import { Index } from "polop/ast/index";
import { NumberLiteral } from "polop/ast/number_literal";
import { Return } from "polop/ast/return";
import { Sequence } from "polop/ast/sequence";
import { StringLiteral } from "polop/ast/string_literal";
import { TupleLiteral } from "polop/ast/tuple_literal";
import { UnaryOperator } from "polop/ast/unary_operator";
import { UndefinedLiteral } from "polop/ast/undefined_literal";
import { VariableReference } from "polop/ast/variable";


export class GatherVariables implements ExpressionVisitor {

  public static process(expression: Expression): Set<string> {
    return expression.accept(new GatherVariables());
  }

  public visitArrayLiteral(expression: ArrayLiteral): Set<string> {
    return this.analyse(expression.values.toSeq());
  }

  public visitAssign(expression: Assign): Set<string> {
    return expression.value.accept(this);
  }

  public visitBinaryOperator(expression: BinaryOperator): Set<string> {
    return expression.left.accept(this).union(expression.right.accept(this));
  }

  public visitBooleanLiteral(_: BooleanLiteral): Set<string> {
    return Set.of();
  }

  public visitDictLiteral(expression: DictLiteral): Set<string> {
    return this.analyse(expression.values.keySeq()).union(this.analyse(expression.values.valueSeq()));
  }

  public visitFunctionCall(expression: FunctionCall): Set<string> {
    return this.analyse(expression.args.toSeq());
  }

  public visitFunctionDefinition(expression: FunctionDefinition): Set<string> {
    return expression.body.accept(this);
  }

  public visitIfThenElse(expression: IfThenElse): Set<string> {
    return expression.condition.accept(this).union(
      expression.whenTrue.accept(this),
      expression.whenFalse.accept(this));
  }

  public visitIndex(expression: Index): Set<string> {
      return expression.value.accept(this).union(expression.index.accept(this));
  }

  public visitNumberLiteral(_: NumberLiteral): Set<string> {
    return Set.of();
  }

  public visitReturn(expression: Return): Set<string> {
    return expression.value.accept(this);
  }

  public visitSequence(expression: Sequence): Set<string> {
    return this.analyse(expression.children.toSeq());
  }

  public visitStringLiteral(_: StringLiteral): Set<string> {
    return Set.of();
  }

  public visitTupleLiteral(expression: TupleLiteral): Set<string> {
    return this.analyse(expression.values.toSeq());
  }

  public visitUndefinedLiteral(_: UndefinedLiteral): Set<string> {
    return Set.of();
  }

  public visitUnaryOperator(expression: UnaryOperator): Set<string> {
    return expression.child.accept(this);
  }

  public visitVariableReference(expression: VariableReference): Set<string> {
    return Set.of(expression.name);
  }

  private analyse(children: Seq.Indexed<Expression>): Set<string> {
    return children.map((e) => e.accept(this)).flatten(true).toSet();
  }
}
