import { ArrayLiteral } from "polop/ast/array_literal";
import { Assign } from "polop/ast/assign";
import { BinaryOperator, BinaryOperators } from "polop/ast/binary_operator";
import { BooleanLiteral } from "polop/ast/boolean_literal";
import { DictLiteral } from "polop/ast/dict_literal";
import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";
import { FunctionCall } from "polop/ast/function_call";
import { FunctionDefinition } from "polop/ast/function_definition";
import { IfThenElse } from "polop/ast/if_then_else";
import { Index } from "polop/ast/index";
import { NumberLiteral } from "polop/ast/number_literal";
import { Return } from "polop/ast/return";
import { Sequence } from "polop/ast/sequence";
import { StringLiteral } from "polop/ast/string_literal";
import { TupleLiteral } from "polop/ast/tuple_literal";
import { UnaryOperator, UnaryOperators } from "polop/ast/unary_operator";
import { UndefinedLiteral } from "polop/ast/undefined_literal";
import { VariableReference } from "polop/ast/variable";


function renderUnaryOperator(operator: UnaryOperators): string {
  switch (operator) {
    case UnaryOperators.NEG: return "neg";
    case UnaryOperators.NOT: return "not";
    case UnaryOperators.SQRT: return "sqrt";
  }
}

function renderBinaryOperator(operator: BinaryOperators): string {
  switch (operator) {
    case BinaryOperators.ADDITION: return "add";
    case BinaryOperators.AND: return "and";
    case BinaryOperators.EQUAL: return "eq";
    case BinaryOperators.FLOAT_DIVISION: return "fdiv";
    case BinaryOperators.GREATER_OR_EQUAL: return "ge";
    case BinaryOperators.GREATER_THAN: return "gt";
    case BinaryOperators.INTEGER_DIVISION: return "idiv";
    case BinaryOperators.LESS_OR_EQUAL: return "le";
    case BinaryOperators.LESS_THAN: return "lt";
    case BinaryOperators.MODULO: return "mod";
    case BinaryOperators.MULTIPLICATION: return "mul";
    case BinaryOperators.NOT_EQUAL: return "neq";
    case BinaryOperators.OR: return "or";
    case BinaryOperators.SUBSTRACTION: return "sub";
    case BinaryOperators.XOR: return "xor";
  }
}

export class Render implements ExpressionVisitor {
  public static process(expression: Expression): string {
    return expression.accept(new Render());
  }

  public visitArrayLiteral(expression: ArrayLiteral): string {
    return `array(${expression.values.map((e) => e.accept(this)).join(", ")})`;
  }

  public visitAssign(expression: Assign): string {
    return `assign(${expression.name}, ${expression.value})`;
  }

  public visitBinaryOperator(expression: BinaryOperator): string {
    return renderBinaryOperator(expression.operator) +
      `(${expression.left.accept(this)}, ${expression.right.accept(this)})`;
  }

  public visitBooleanLiteral(expression: BooleanLiteral): string {
    return `literal(${ expression.value })`;
  }

  public visitDictLiteral(expression: DictLiteral): string {
    return `dict(${expression.values.map((v, k) => `${k.accept(this)}: ${v.accept(this)}`).join(", ")})`;
  }

  public visitFunctionCall(expression: FunctionCall): string {
    return expression.functor +
      `(${ expression.args.map((a) => a.accept(this)).join(", ")})`;
  }

  public visitFunctionDefinition(expression: FunctionDefinition): string {
    return `function([${expression.parameters.join(", ")}], ${expression.body.accept(this)})`;
  }

  public visitIfThenElse(expression: IfThenElse): string {
    return `ifte(
      ${expression.condition.accept(this)},
      ${expression.whenTrue.accept(this)},
      ${expression.whenFalse.accept(this)})`;
  }

  public visitIndex(expression: Index): string {
      return `index(${expression.value.accept(this)}, ${expression.index.accept(this)})`;
  }

  public visitNumberLiteral(expression: NumberLiteral): string {
    return `literal(${ expression.value })`;
  }

  public visitReturn(expression: Return): string {
    return `ret(${expression.value.accept(this)})`;
  }

  public visitSequence(expression: Sequence): string {
    return `seq(${expression.children.map((e) => e.accept(this)).join(", ")})`;
  }

  public visitStringLiteral(expression: StringLiteral): string {
    return `str(${expression.value})`;
  }

  public visitTupleLiteral(expression: TupleLiteral): string {
    return `tuple(${expression.values.map((e) => e.accept(this)).join(", ")})`;
  }

  public visitUndefinedLiteral(_: UndefinedLiteral): string {
    return "literal(undefined)";
  }

  public visitUnaryOperator(expression: UnaryOperator): string {
    return renderUnaryOperator(expression.operator) +
      `(${expression.child.accept(this)})`;
  }

  public visitVariableReference(expression: VariableReference): string {
    return `variable(${ expression.name })`;
  }
}
