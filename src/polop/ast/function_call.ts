import { List } from "immutable";

import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class FunctionCall implements Expression {
  constructor(
    public readonly functor: Expression,
    public readonly args: List<Expression>) {
  }

  public isConstant(): boolean {
    return this.args.map((e) => e.isConstant()).reduce((p, v) => p && v);
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitFunctionCall(this);
  }

  public changeFunctor(functor: Expression): FunctionCall {
    return new FunctionCall(functor, this.args);
  }

  public changeArgs(args: List<Expression>): FunctionCall {
    return new FunctionCall(this.functor, args);
  }
}
