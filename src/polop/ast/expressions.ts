import { List, Map } from "immutable";

import { ArrayLiteral } from "polop/ast/array_literal";
import { Assign } from "polop/ast/assign";
import { BinaryOperator, BinaryOperators } from "polop/ast/binary_operator";
import { BooleanLiteral } from "polop/ast/boolean_literal";
import { DictLiteral } from "polop/ast/dict_literal";
import { Expression } from "polop/ast/expression";
import { FunctionCall } from "polop/ast/function_call";
import { FunctionDefinition } from "polop/ast/function_definition";
import { IfThenElse } from "polop/ast/if_then_else";
import { Index } from "polop/ast/index";
import { NumberLiteral } from "polop/ast/number_literal";
import { Return } from "polop/ast/return";
import { Sequence } from "polop/ast/sequence";
import { StringLiteral } from "polop/ast/string_literal";
import { TupleLiteral } from "polop/ast/tuple_literal";
import { UnaryOperator, UnaryOperators } from "polop/ast/unary_operator";
import { UndefinedLiteral } from "polop/ast/undefined_literal";
import { VariableReference } from "polop/ast/variable";

export function literal(value: string|number|boolean|undefined): Expression {
  if (typeof value === "string") {
    return new StringLiteral(value);
  }
  if (typeof value === "number") {
    return new NumberLiteral(value);
  }
  if (typeof value === "boolean") {
    return new BooleanLiteral(value);
  }
  if (typeof value === "undefined") {
    return new UndefinedLiteral();
  }
}

export function dict(...keyvalues: Expression[]): Expression {
  return new DictLiteral(Map.of(...keyvalues));
}

export function array(...values: Expression[]): Expression {
  return new ArrayLiteral(List.of(...values));
}

export function tuple(...values: Expression[]): Expression {
  return new TupleLiteral(List.of(...values));
}

export function index(value: Expression, index: Expression): Expression {
  return new Index(value, index);
}

export function assign(name: string, value: Expression): Expression {
  return new Assign(name, value);
}

export function ret(value: Expression): Expression {
  return new Return(value);
}

export function if_then(
  condition: Expression,
  whenTrue: Expression,
  whenFalse: Expression = literal(undefined)): Expression {
  return new IfThenElse(condition, whenTrue, whenFalse);
}

export function seq(...children: Expression[]): Expression {
  return new Sequence(List.of(...children));
}

function processParams(parameters: string | string[] | List<string>): List<string> {
  if (typeof parameters === "string") {
    return List.of(...parameters.split(" "));
  }
  if (List.isList(parameters)) {
    return parameters;
  }
  return List.of(...parameters);
}

export function func(parameters: string | string[] | List<string>, body: Expression): Expression {
  return new FunctionDefinition(processParams(parameters), body);
}

export function call(functor: Expression, ...values: Expression[]): Expression {
  return new FunctionCall(functor, List.of(...values));
}

export function combine(binary: (l: Expression, r: Expression) => Expression, ...operands: Expression[]): Expression {
  return operands.reduce(binary);
}

export function eq(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.EQUAL, left, right);
}

export function concat(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.CONCAT, left, right);
}

export function neq(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.NOT_EQUAL, left, right);
}

export function lt(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.LESS_THAN, left, right);
}

export function le(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.LESS_OR_EQUAL, left, right);
}

export function gt(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.GREATER_THAN, left, right);
}

export function ge(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.GREATER_OR_EQUAL, left, right);
}

export function add(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.ADDITION, left, right);
}

export function sub(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.SUBSTRACTION, left, right);
}

export function mul(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.MULTIPLICATION, left, right);
}

export function fdiv(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.FLOAT_DIVISION, left, right);
}

export function idiv(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.INTEGER_DIVISION, left, right);
}

export function mod(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.MODULO, left, right);
}

export function and(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.AND, left, right);
}

export function or(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.OR, left, right);
}

export function xor(left: Expression, right: Expression): Expression {
  return new BinaryOperator(BinaryOperators.XOR, left, right);
}

export function len(child: Expression): Expression {
  return new UnaryOperator(UnaryOperators.LEN, child);
}

export function str(child: Expression): Expression {
  return new UnaryOperator(UnaryOperators.STR, child);
}

export function bool(child: Expression): Expression {
  return new UnaryOperator(UnaryOperators.BOOL, child);
}

export function not(child: Expression): Expression {
  return new UnaryOperator(UnaryOperators.NOT, child);
}

export function neg(child: Expression): Expression {
  return new UnaryOperator(UnaryOperators.NEG, child);
}

export function sqrt(child: Expression): Expression {
  return new UnaryOperator(UnaryOperators.SQRT, child);
}

export function variable(name: string): Expression {
  return new VariableReference(name);
}
