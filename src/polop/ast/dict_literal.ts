import { Map } from "immutable";

import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class DictLiteral implements Expression {
  constructor(public readonly values: Map<Expression, Expression>) {
  }

  public isConstant(): boolean {
    return true;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitDictLiteral(this);
  }

  public changeValues(values: Map<Expression, Expression>): DictLiteral {
    return new DictLiteral(values);
  }
}
