import { List } from "immutable";

import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class FunctionDefinition implements Expression {
  constructor(
    public readonly parameters: List<string>,
    public readonly body: Expression) {
  }

  public isConstant(): boolean {
    return this.body.isConstant();
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitFunctionDefinition(this);
  }

  public changeBody(body: Expression): FunctionDefinition {
    return new FunctionDefinition(this.parameters, body);
  }
}
