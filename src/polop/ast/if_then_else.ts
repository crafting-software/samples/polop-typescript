import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class IfThenElse implements Expression {
  constructor(
    public readonly condition: Expression,
    public readonly whenTrue: Expression,
    public readonly whenFalse: Expression) {
  }

  public isConstant(): boolean {
    return this.condition.isConstant();
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitIfThenElse(this);
  }

  public changeCondition(expression: Expression): IfThenElse {
    return new IfThenElse(expression, this.whenTrue, this.whenFalse);
  }

  public changeWhenTrue(expression: Expression): IfThenElse {
    return new IfThenElse(this.condition, expression, this.whenFalse);
  }

  public changeWhenFalse(expression: Expression): IfThenElse {
    return new IfThenElse(this.condition, this.whenTrue, expression);
  }
}
