import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export class BooleanLiteral implements Expression {
  constructor(public readonly value: boolean) {
  }

  public isConstant(): boolean {
    return true;
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitBooleanLiteral(this);
  }
}
