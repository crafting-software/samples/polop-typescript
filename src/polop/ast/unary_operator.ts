import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";


export enum UnaryOperators {
  BOOL,
  LEN,
  NEG,
  NOT,
  SQRT,
  STR,
}


export class UnaryOperator implements Expression {
  constructor(
    public readonly operator: UnaryOperators,
    public readonly child: Expression) {
  }

  public isConstant(): boolean {
    return this.child.isConstant();
  }

  public accept(visitor: ExpressionVisitor): any {
    return visitor.visitUnaryOperator(this);
  }

  public changeOperator(operator: UnaryOperators): UnaryOperator {
    return new UnaryOperator(operator, this.child);
  }

  public changeChild(child: Expression): UnaryOperator {
    return new UnaryOperator(this.operator, child);
  }
}
