import { ArrayLiteral } from "polop/ast/array_literal";
import { Assign } from "polop/ast/assign";
import { BinaryOperator } from "polop/ast/binary_operator";
import { BooleanLiteral } from "polop/ast/boolean_literal";
import { DictLiteral } from "polop/ast/dict_literal";
import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";
import { literal } from "polop/ast/expressions";
import { FunctionCall } from "polop/ast/function_call";
import { FunctionDefinition } from "polop/ast/function_definition";
import { IfThenElse } from "polop/ast/if_then_else";
import { Index } from "polop/ast/index";
import { NumberLiteral } from "polop/ast/number_literal";
import { Return } from "polop/ast/return";
import { Sequence } from "polop/ast/sequence";
import { StringLiteral } from "polop/ast/string_literal";
import { TupleLiteral } from "polop/ast/tuple_literal";
import { UnaryOperator } from "polop/ast/unary_operator";
import { UndefinedLiteral } from "polop/ast/undefined_literal";
import { VariableReference } from "polop/ast/variable";
import { Evaluator } from "polop/interpreter/evaluator";
import * as v from "polop/interpreter/values";


function asLiteral(value: v.Value): Expression {
  if (value instanceof v.Number) {
    return literal(value.value);
  }
  if (value instanceof v.Boolean) {
    return literal(value.value);
  }
  throw new TypeError("invalid type");
}


export class ConstantPropagation implements ExpressionVisitor {
  public static process(expression: Expression): Expression {
    return expression.accept(new ConstantPropagation());
  }

  public visitArrayLiteral(expression: ArrayLiteral): Expression {
    return expression.changeValues(expression.values.map((v) => v.accept(this)));
  }

  public visitAssign(expression: Assign): Expression {
    return expression.changeValue(expression.value.accept(this));
  }

  public visitBinaryOperator(expression: BinaryOperator): Expression {
    const left = expression.left.accept(this);
    const right = expression.right.accept(this);
    if (left.isConstant() && right.isConstant()) {
      return asLiteral(Evaluator.process(expression));
    }
    return expression.changeLeft(left).changeRight(right);
  }

  public visitBooleanLiteral(expression: BooleanLiteral): Expression {
    return expression;
  }

  public visitDictLiteral(expression: DictLiteral): Expression {
    return expression;
  }

  public visitFunctionCall(expression: FunctionCall): Expression {
    return expression.changeArgs(expression.args.map((a) => a.accept(this)));
  }

  public visitFunctionDefinition(expression: FunctionDefinition): Expression {
    return expression.changeBody(expression.body.accept(this));
  }

  public visitIfThenElse(expression: IfThenElse): Expression {
    const condition = expression.condition.accept(this);
    const whenTrue = expression.whenTrue.accept(this);
    const whenFalse = expression.whenFalse.accept(this);
    if (condition.isConstant()) {
      if (Evaluator.process(condition).asBoolean()) {
        return whenTrue;
      } else {
        return whenFalse;
      }
    } else {
      return expression.
        changeCondition(condition).
        changeWhenTrue(whenTrue).
        changeWhenFalse(whenFalse);
    }
  }

  public visitIndex(expression: Index): Expression {
    const value = expression.value.accept(this);
    const index = expression.index.accept(this);
    if (value.isConstant() && index.isConstant()) {
      return asLiteral(Evaluator.process(expression));
    }
    return expression.changeValue(value).changeIndex(index);
  }

  public visitNumberLiteral(expression: NumberLiteral): Expression {
    return expression;
  }

  public visitReturn(expression: Return): Expression {
    return expression.changeValue(expression.value.accept(this));
  }

  public visitSequence(expression: Sequence): Expression {
    return expression.changeChildren(expression.children.map((v) => v.accept(this)));
  }

  public visitStringLiteral(expression: StringLiteral): Expression {
    return expression;
  }

  public visitTupleLiteral(expression: TupleLiteral): Expression {
    return expression.changeValues(expression.values.map((v) => v.accept(this)));
  }

  public visitUnaryOperator(expression: UnaryOperator): Expression {
    const child = expression.child.accept(this);
    if (child.isConstant()) {
      return asLiteral(Evaluator.process(expression));
    }
    return expression.changeChild(child);
  }

  public visitUndefinedLiteral(expression: UndefinedLiteral): Expression {
    return expression;
  }

  public visitVariableReference(expression: VariableReference): Expression {
    return expression;
  }
}
