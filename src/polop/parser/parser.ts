import { List, Map } from "immutable";

import { Expression } from "polop/ast/expression";
import * as a from "polop/ast/expressions";


export interface Message {
  error(content: string): void;
}

export class ConsoleMessage implements Message {
  public error(content: string): void {
    // tslint:disable-next-line:no-console
    console.log(content);
  }
}


export class Parser {

  public static parse(content: string): Expression {
    return new Parser(content).parse();
  }

  public position: number = 0;
  public collector: List<string> = List.of();

  public constructor(public readonly content: string) { }

  public messages(): List<string> {
    return this.collector;
  }

  public parse(): Expression {
    return null;
  }

  public boolean(): Expression {
    if (this.check_token("true")) {
      return a.literal(true);
    }
    if (this.check_token("false")) {
      return a.literal(false);
    }
    this.error("boolean");
  }

  public number(): Expression {
    const value = this.pattern(/^[-+]?[0-9]*\.?[0-9]+([E][-+]?[0-9]+)?/i);
    if (value !== null) {
      this.advance(value.length);
      return a.literal(parseInt(value, 10));
    }
    this.error("number");
  }

  public eos(): boolean {
    return this.position >= this.content.length;
  }

  public spaces() {
    while (!this.eos() && this.current() === " ") {
      this.advance(1);
    }
  }

  public current(): string {
    return this.content.charAt(this.position);
  }

  public tail(): string {
    return this.content.substring(this.position);
  }

  public advance(count: number): void {
    this.position += count;
  }

  public pattern(pattern: RegExp|string): string {
    const match = this.tail().match(pattern);
    if (match !== null) {
      return match[0];
    }
    return null;
  }

  public request_token(token: string) {
    if (this.content.startsWith(token, this.position)) {
      this.advance(token.length);
    } else {
      this.error(token);
    }
  }

  public check_token(token: string): boolean {
    if (this.content.startsWith(token, this.position)) {
      this.advance(token.length);
      return true;
    } else {
      return false;
    }
  }

  public error(token: string): void {
    const message = `Expected '${token}' at #${this.position} got '${this.tail().slice(0, 20)}'`;
    throw new Error(message);
  }
}
