import * as v from "polop/interpreter/values";

export interface Lazy {
  get(): v.Value;
}

export function value(value: v.Value): Lazy {
  return { get: (): v.Value => value };
}
