import { ArrayLiteral } from "polop/ast/array_literal";
import { Assign } from "polop/ast/assign";
import { BinaryOperator, BinaryOperators } from "polop/ast/binary_operator";
import { BooleanLiteral } from "polop/ast/boolean_literal";
import { DictLiteral } from "polop/ast/dict_literal";
import { Expression } from "polop/ast/expression";
import { ExpressionVisitor } from "polop/ast/expression_visitor";
import { FunctionCall } from "polop/ast/function_call";
import { FunctionDefinition } from "polop/ast/function_definition";
import { IfThenElse } from "polop/ast/if_then_else";
import { Index } from "polop/ast/index";
import { NumberLiteral } from "polop/ast/number_literal";
import { GatherVariables } from "polop/ast/operations/gather_variables";
import { Return } from "polop/ast/return";
import { Sequence } from "polop/ast/sequence";
import { StringLiteral } from "polop/ast/string_literal";
import { TupleLiteral } from "polop/ast/tuple_literal";
import { UnaryOperator, UnaryOperators } from "polop/ast/unary_operator";
import { UndefinedLiteral } from "polop/ast/undefined_literal";
import { VariableReference } from "polop/ast/variable";
import { Frame } from "polop/interpreter/frame";
import { Lazy, value} from "polop/interpreter/lazy";
import * as v from "polop/interpreter/values";


export class Evaluator implements ExpressionVisitor {

  public static process(expression: Expression, frame: Frame = Frame.empty()): v.Value {
    return expression.accept(new Evaluator(frame));
  }

  private running: boolean = true;
  private returnValue: v.Value = v.Undefined.of();

  constructor(private frame: Frame) {
  }

  public visitArrayLiteral(expression: ArrayLiteral): v.Value {
    return v.Array.of(expression.values.map((a) => this.eval(a)));
  }

  public visitAssign(expression: Assign): v.Value {
    const content: v.Value = this.eval(expression.value);
    this.frame.set(expression.name, value(content));
    return content;
  }

  public visitBinaryOperator(expression: BinaryOperator): v.Value {
    const left = this.eval(expression.left);
    const right = () => this.eval(expression.right);
    switch (expression.operator) {
      case BinaryOperators.ADDITION: return v.Number.of(left.asNumber() + right().asNumber());
      case BinaryOperators.AND: {
        if (left.asBoolean()) {
          return right();
        }
        return v.Boolean.of(false);
      }
      case BinaryOperators.CONCAT: return left.concat(right());
      case BinaryOperators.EQUAL: return v.Boolean.of(left.repr() === right().repr());
      case BinaryOperators.FLOAT_DIVISION: return v.Number.of(left.asNumber() / right().asNumber());
      case BinaryOperators.GREATER_OR_EQUAL: return v.Boolean.of(left.asNumber() >= right().asNumber());
      case BinaryOperators.GREATER_THAN: return v.Boolean.of(left.asNumber() > right().asNumber());
      case BinaryOperators.INTEGER_DIVISION: return v.Number.of(Math.floor(left.asNumber() / right().asNumber()));
      case BinaryOperators.LESS_OR_EQUAL: return v.Boolean.of(left.asNumber() <= right().asNumber());
      case BinaryOperators.LESS_THAN: return v.Boolean.of(left.asNumber() < right().asNumber());
      case BinaryOperators.MODULO: return v.Number.of(left.asNumber() % right().asNumber());
      case BinaryOperators.MULTIPLICATION: return v.Number.of(left.asNumber() * right().asNumber());
      case BinaryOperators.NOT_EQUAL: return v.Boolean.of(left.repr() !== right().repr());
      case BinaryOperators.OR: {
        if (left.asBoolean()) {
          return v.Boolean.of(true);
        }
        return right();
      }
      case BinaryOperators.SUBSTRACTION: return v.Number.of(left.asNumber() - right().asNumber());
      case BinaryOperators.XOR: return v.Boolean.of(left.asBoolean() ? !right().asBoolean() : right().asBoolean());
    }
    return v.Undefined.of();
  }

  public visitBooleanLiteral(expression: BooleanLiteral): v.Value {
    return v.Boolean.of(expression.value);
  }

  public visitDictLiteral(expression: DictLiteral): v.Value {
    const dict = v.Dict.empty();
    expression.values.forEach((v, k) => {
      dict.set(this.eval(k).asString(), this.eval(v));
    });
    return dict;
  }

  public visitFunctionCall(expression: FunctionCall): v.Value {
    const functor = this.eval(expression.functor).asFunctor();
    if (expression.args.size < functor.parameters().size) {
      // currification
      throw new TypeError("too less arguments");
    } else if (expression.args.size === functor.parameters().size) {
      return Evaluator.process(
        functor.body(),
        functor.evaluationFrame(expression.args.map((a) => this.lazy(a))));
    }
    throw new TypeError("too many arguments");
  }

  public visitFunctionDefinition(expression: FunctionDefinition): v.Value {
    return v.Functor.of(
      expression,
      this.frame.closure(GatherVariables.process(expression)));
  }

  public visitIfThenElse(expression: IfThenElse): v.Value {
    if (this.eval(expression.condition).asBoolean()) {
      return this.eval(expression.whenTrue);
    } else {
      return this.eval(expression.whenFalse);
    }
  }

  public visitIndex(expression: Index): v.Value {
    const values = this.eval(expression.value);
    if (values.isCollection()) {
      return values.asCollection().get(this.eval(expression.index).asNumber());
    }
    if (values.isDict()) {
      return values.asDict().get(this.eval(expression.index).asString());
    }
    throw new TypeError("invalid type");
  }

  public visitNumberLiteral(expression: NumberLiteral): v.Value {
    return v.Number.of(expression.value);
  }

  public visitReturn(expression: Return): v.Value {
    this.returnValue = this.eval(expression.value);
    this.running = false;
    return this.returnValue;
  }

  public visitSequence(expression: Sequence): v.Value {
    return expression.children.map((e) => this.eval(e)).last();
  }

  public visitStringLiteral(expression: StringLiteral): v.Value {
    return v.String.of(expression.value);
  }

  public visitTupleLiteral(expression: TupleLiteral): v.Value {
    return v.Tuple.of(expression.values.map((a) => this.eval(a)));
  }

  public visitUnaryOperator(expression: UnaryOperator): v.Value {
    const child: v.Value = this.eval(expression.child);
    switch (expression.operator) {
      case UnaryOperators.BOOL: return v.Boolean.of(child.asBoolean());
      case UnaryOperators.LEN: return v.Number.of(child.len());
      case UnaryOperators.NEG: return v.Number.of(-child.asNumber());
      case UnaryOperators.NOT: return v.Boolean.of(!child.asBoolean());
      case UnaryOperators.SQRT: return v.Number.of(Math.sqrt(child.asNumber()));
      case UnaryOperators.STR: return v.String.of(child.str());
    }
    return v.Undefined.of();
  }

  public visitUndefinedLiteral(_: UndefinedLiteral): v.Value {
    return v.Undefined.of();
  }

  public visitVariableReference(expression: VariableReference): v.Value {
    return this.frame.get(expression.name).get();
  }

  private eval(expression: Expression): v.Value {
    if (this.running) {
      return expression.accept(this);
    }
    return this.returnValue;
  }

  private lazy(expression: Expression): Lazy {
    return { get: () => this.eval(expression) };
  }
}
