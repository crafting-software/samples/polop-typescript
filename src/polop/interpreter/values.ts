import { List, Map } from "immutable";

import { Expression } from "polop/ast/expression";
import { FunctionDefinition } from "polop/ast/function_definition";
import { Frame } from "polop/interpreter/frame";
import { Lazy} from "polop/interpreter/lazy";


export interface Value {
  repr(): string;
  str(): string;
  asBoolean(): boolean;
  asNumber(): number;
  asString(): string;
  asFunctor(): Functor;
  asCollection(): List<Value>;
  asDict(): Dict;

  isCollection(): boolean;
  isDict(): boolean;

  concat(other: Value): Value;
  len(): number;
}

export class Undefined implements Value {
  public static of(): Value {
    return Undefined.singleton;
  }

  private static singleton: Value = new Undefined();
  private constructor() {}

  public repr(): string { return "<undefined>"; }
  public str(): string { return "undefined"; }
  public asBoolean(): boolean { return false; }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { throw new TypeError("invalid type"); }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return false; }
  public isDict(): boolean { return false; }
  public concat(_: Value): Value { throw new TypeError("invalid type"); }
  public len(): number {throw new TypeError("invalid type"); }
}

export class String implements Value {
  public static of(value: string): Value {
    // tslint:disable-next-line:no-construct
    return new String(value);
  }

  private constructor(public readonly value: string) {}

  public repr(): string { return `'${this.value}'`; }
  public str(): string { return this.value; }
  public asBoolean(): boolean { return this.value.length > 0; }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { return this.value; }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { throw new TypeError("invalid type"); }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return false; }
  public isDict(): boolean { return false; }
  public concat(other: Value): Value { return String.of(this.value + other.str()); }
  public len(): number { return this.value.length; }
}

export class Number implements Value {
  public static of(value: number): Value {
    // tslint:disable-next-line:no-construct
    return new Number(value);
  }

  private constructor(public readonly value: number) {}

  public repr(): string { return this.str(); }
  public str(): string { return this.value.toString(); }
  public asBoolean(): boolean { return this.value > 0; }
  public asNumber(): number { return this.value; }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { throw new TypeError("invalid type"); }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return false; }
  public isDict(): boolean { return false; }
  public concat(_: Value): Value { throw new TypeError("invalid type"); }
  public len(): number {throw new TypeError("invalid type"); }
}

export class Boolean implements Value {
  public static of(value: boolean): Value {
    // tslint:disable-next-line:no-construct
    return new Boolean(value);
  }

  private constructor(public readonly value: boolean) { }

  public repr(): string { return this.str(); }
  public str(): string { return this.value ? "true" : "false"; }
  public asBoolean(): boolean { return this.value; }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { throw new TypeError("invalid type"); }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return false; }
  public isDict(): boolean { return false; }
  public concat(_: Value): Value { throw new TypeError("invalid type"); }
  public len(): number {throw new TypeError("invalid type"); }
}

export class Array implements Value {
  public static of(values: List<Value>): Value {
    return new Array(values);
  }

  private constructor(public readonly values: List<Value>) { }

  public repr(): string { return this.str(); }
  public str(): string { return `[${this.values.map((v) => v.repr()).join(", ")}]`; }
  public asBoolean(): boolean { return !this.values.isEmpty(); }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { return this.values; }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return true; }
  public isDict(): boolean { return false; }
  public concat(other: Value): Value {
    return Array.of(this.values.concat(other.asCollection()));
  }
  public len(): number { return this.values.size; }
}

export class Tuple implements Value {
  public static of(values: List<Value>): Value {
    return new Tuple(values);
  }

  private constructor(public readonly values: List<Value>) { }

  public repr(): string { return this.str(); }
  public str(): string { return `(${this.values.map((v) => v.repr()).join(", ")})`; }
  public asBoolean(): boolean { return !this.values.isEmpty(); }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { return this.values; }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return true; }
  public isDict(): boolean { return false; }
  public concat(other: Value): Value {
    return Tuple.of(this.values.concat(other.asCollection()));
  }
  public len(): number { return this.values.size; }
}

export class Dict implements Value {
  public static empty(): Dict {
    return new Dict(Map.of());
  }

  public static of(values: Map<string, Value>): Value {
    return new Dict(values);
  }

  private values: Map<string, Value>;

  private constructor(values: Map<string, Value>) { this.values = values; }

  public repr(): string { return this.str(); }
  public str(): string { return `{${this.values.entrySeq().sort().map((e) => `${e[0]}: ${e[1].repr()}`).join(", ")}}`; }
  public asBoolean(): boolean { return !this.values.isEmpty(); }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { throw new TypeError("invalid type"); }
  public asCollection(): List<Value> { throw new TypeError("invalid type"); }
  public asDict(): Dict { return this; }
  public isCollection(): boolean { return false; }
  public isDict(): boolean { return true; }
  public concat(other: Value): Value {
    return Dict.of(this.values.concat(other.asDict().values));
  }
  public len(): number { return this.values.size; }

  public set(key: string, value: Value): Dict {
    this.values = this.values.set(key, value);
    return this;
  }

  public get(key: string): Value {
    return this.values.get(key);
  }
}


export class Functor implements Value {
  public static of(definition: FunctionDefinition, frame: Frame): Value {
    return new Functor(definition, frame);
  }

  private constructor(
    public readonly definition: FunctionDefinition,
    public readonly frame: Frame) {
  }

  public repr(): string { return this.str(); }
  public str(): string { return `<func(${this.parameters().join(", ")})>`; }
  public asBoolean(): boolean { throw new TypeError("invalid type"); }
  public asNumber(): number { throw new TypeError("invalid type"); }
  public asString(): string { throw new TypeError("invalid type"); }
  public asFunctor(): Functor { return this; }
  public asCollection(): List<Value> { throw new TypeError("invalid type"); }
  public asDict(): Dict { throw new TypeError("invalid type"); }
  public isCollection(): boolean { return false; }
  public isDict(): boolean { return false; }
  public concat(_: Value): Value { throw new TypeError("invalid type"); }
  public len(): number {throw new TypeError("invalid type"); }

  public parameters(): List<string> {
    return this.definition.parameters;
  }

  public body(): Expression {
    return this.definition.body;
  }

  public evaluationFrame(args: List<Lazy>): Frame {
    const result = this.frame.duplicate();
    this.parameters().zip(args).forEach((e) => result.set(e[0], e[1]));
    return result;
  }
}
