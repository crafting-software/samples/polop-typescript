import { Map, Set } from "immutable";

import { Lazy, value} from "polop/interpreter/lazy";
import * as v from "polop/interpreter/values";


export class Frame {
  public static empty(): Frame {
    return new Frame();
  }

  private constructor(private symbols: Map<string, Lazy> = Map.of()) {
    this.symbols = symbols;
  }

  public get(symbol: string): Lazy {
    return this.symbols.get(symbol, value(v.Undefined.of()));
  }

  public set(symbol: string, value: Lazy): Frame {
      this.symbols = this.symbols.set(symbol, value);
      return this;
  }

  public closure(symbols: Set<string>): Frame {
    const result = new Frame();
    symbols.forEach((symbol) => { result.set(symbol, this.get(symbol)); });
    return result;
  }

  public duplicate(): Frame {
    return new Frame(this.symbols);
  }
}
