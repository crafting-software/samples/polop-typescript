import { expect } from "chai";

import { Expression } from "polop/ast/expression";
import * as a from "polop/ast/expressions";
import { Render } from "polop/ast/operations/render";
import { ConstantPropagation } from "polop/optimization/constant_propagation";


describe("constant propagation", () => {
  function simplify(expression: Expression): string {
    return Render.process(ConstantPropagation.process(expression));
  }

  it("should keep integer literal", () => {
    expect(simplify(a.literal(42))).to.equal("literal(42)");
  });

  it("should keep boolean literal", () => {
    expect(simplify(a.literal(true))).to.equal("literal(true)");
    expect(simplify(a.literal(false))).to.equal("literal(false)");
  });

  it("should keep a variable", () => {
    expect(simplify(a.variable("toto"))).to.equal("variable(toto)");
  });

  it("should simplify a binary operation when constants", () => {
    expect(simplify(a.lt(a.literal(42), a.literal(2)))).to.equal("literal(false)");
    expect(simplify(a.add(a.literal(42), a.literal(2)))).to.equal("literal(44)");
    expect(simplify(a.and(a.literal(true), a.literal(false)))).to.equal("literal(false)");
  });

  it("should keep a binary operation when variable", () => {
    expect(simplify(a.add(a.literal(42), a.variable("toto")))).to.equal("add(literal(42), variable(toto))");
  });
});
