import { expect } from "chai";

import { Expression } from "polop/ast/expression";
import * as a from "polop/ast/expressions";
import { Evaluator } from "polop/interpreter/evaluator";
import { Frame } from "polop/interpreter/frame";
import { value} from "polop/interpreter/lazy";
import * as v from "polop/interpreter/values";


type FrameSetter = (frame: Frame) => void;

function def(name: string, v: v.Value): FrameSetter {
  return (frame: Frame) => { frame.set(name, value(v)); };
}

function frameWith(defines: FrameSetter[]): Frame {
  const context = Frame.empty();
  defines.forEach((d) => d(context));
  return context;
}


describe("interpreter", () => {

  function evaluate(expression: Expression, ...defines: FrameSetter[]): string {
    return Evaluator.process(expression, frameWith(defines)).repr();
  }

  function marker(expression: Expression, collector: any[]): Expression {
    return {
      accept: (visitor) => {
        collector.push(true);
        return expression.accept(visitor);
      },
      isConstant: expression.isConstant,
    };
  }


  describe("literals", () => {
    it("should evaluate an undefined literal", () => {
      expect(evaluate(a.literal(undefined))).to.equal("<undefined>");
    });

    it("should evaluate a integer literal", () => {
      expect(evaluate(a.literal(42))).to.equal("42");
    });

    it("should evaluate a boolean literal", () => {
      expect(evaluate(a.literal(true))).to.equal("true");
      expect(evaluate(a.literal(false))).to.equal("false");
    });

    it("should evaluate a string literal", () => {
      expect(evaluate(a.literal("value"))).to.equal("'value'");
    });

    it("should evaluate a tuple literal", () => {
      expect(evaluate(a.tuple(a.literal(true), a.literal(42)))).to.equal("(true, 42)");
    });

    it("should evaluate an array literal", () => {
      expect(evaluate(a.array(a.literal(10), a.literal(42)))).to.equal("[10, 42]");
    });

    it("should evaluate a dict literal", () => {
      expect(evaluate(a.dict(a.literal("x"), a.literal(42), a.literal("y"), a.literal(24)))).to.equal("{x: 42, y: 24}");
    });
  });

  describe("arithmetic operators", () => {
    it("should evaluate a addition", () => {
      expect(evaluate(a.add(a.literal(42), a.literal(0)))).to.equal("42");
      expect(evaluate(a.combine(a.add, a.literal(1), a.literal(2), a.literal(3)))).to.equal("6");
    });

    it("should evaluate a substraction", () => {
      expect(evaluate(a.sub(a.literal(43), a.literal(1)))).to.equal("42");
    });

    it("should evaluate a multiplication", () => {
      expect(evaluate(a.mul(a.literal(21), a.literal(2)))).to.equal("42");
    });

    it("should evaluate a division", () => {
      expect(evaluate(a.fdiv(a.literal(84), a.literal(2)))).to.equal("42");
    });

    it("should evaluate a modulo", () => {
      expect(evaluate(a.mod(a.literal(5), a.literal(2)))).to.equal("1");
    });

    it("should evaluate a negation", () => {
      expect(evaluate(a.neg(a.literal(42)))).to.equal("-42");
    });
  });

  describe("logical operators", () => {
    it("should evaluate a logical not", () => {
      expect(evaluate(a.not(a.literal(true)))).to.equal("false");
      expect(evaluate(a.not(a.literal(false)))).to.equal("true");
    });

    it("should evaluate a logical and", () => {
      expect(evaluate(a.and(a.literal(true), a.literal(true)))).to.equal("true");
      expect(evaluate(a.and(a.literal(true), a.literal(false)))).to.equal("false");
      expect(evaluate(a.and(a.literal(false), a.literal(true)))).to.equal("false");
      expect(evaluate(a.and(a.literal(false), a.literal(false)))).to.equal("false");
    });

    it("should evaluate a logical or", () => {
      expect(evaluate(a.or(a.literal(true), a.literal(true)))).to.equal("true");
      expect(evaluate(a.or(a.literal(true), a.literal(false)))).to.equal("true");
      expect(evaluate(a.or(a.literal(false), a.literal(true)))).to.equal("true");
      expect(evaluate(a.or(a.literal(false), a.literal(false)))).to.equal("false");
    });

    it("should evaluate a logical xor", () => {
      expect(evaluate(a.xor(a.literal(true), a.literal(true)))).to.equal("false");
      expect(evaluate(a.xor(a.literal(true), a.literal(false)))).to.equal("true");
      expect(evaluate(a.xor(a.literal(false), a.literal(true)))).to.equal("true");
      expect(evaluate(a.xor(a.literal(false), a.literal(false)))).to.equal("false");
    });
  });

  describe("comparison operators", () => {
    it("should evaluate equal", () => {
      expect(evaluate(a.eq(a.literal(true), a.literal(true)))).to.equal("true");
      expect(evaluate(a.eq(a.literal(true), a.literal(false)))).to.equal("false");
      expect(evaluate(a.eq(a.literal(42), a.literal(42)))).to.equal("true");
      expect(evaluate(a.eq(a.literal(42), a.literal(10)))).to.equal("false");
    });

    it("should evaluate not equal", () => {
      expect(evaluate(a.neq(a.literal(true), a.literal(true)))).to.equal("false");
      expect(evaluate(a.neq(a.literal(true), a.literal(false)))).to.equal("true");
      expect(evaluate(a.neq(a.literal(42), a.literal(42)))).to.equal("false");
      expect(evaluate(a.neq(a.literal(42), a.literal(10)))).to.equal("true");
    });

    it("should evaluate less than", () => {
      expect(evaluate(a.lt(a.literal(42), a.literal(42)))).to.equal("false");
      expect(evaluate(a.lt(a.literal(42), a.literal(10)))).to.equal("false");
      expect(evaluate(a.lt(a.literal(10), a.literal(42)))).to.equal("true");
    });

    it("should evaluate less or equal", () => {
      expect(evaluate(a.le(a.literal(42), a.literal(42)))).to.equal("true");
      expect(evaluate(a.le(a.literal(42), a.literal(10)))).to.equal("false");
      expect(evaluate(a.le(a.literal(10), a.literal(42)))).to.equal("true");
    });

    it("should evaluate greater than", () => {
      expect(evaluate(a.gt(a.literal(42), a.literal(42)))).to.equal("false");
      expect(evaluate(a.gt(a.literal(42), a.literal(10)))).to.equal("true");
      expect(evaluate(a.gt(a.literal(10), a.literal(42)))).to.equal("false");
    });

    it("should evaluate greater or equal", () => {
      expect(evaluate(a.ge(a.literal(42), a.literal(42)))).to.equal("true");
      expect(evaluate(a.ge(a.literal(42), a.literal(10)))).to.equal("true");
      expect(evaluate(a.ge(a.literal(10), a.literal(42)))).to.equal("false");
    });
  });

  describe("concat operations", () => {
    it("should evaluate concatenate two strings", () => {
      expect(evaluate(a.concat(a.literal("Foo"), a.literal("Bar")))).to.equal("'FooBar'");
    });

    it("should evaluate concatenate two tuples", () => {
      expect(evaluate(
        a.concat(
          a.tuple(a.literal(true)),
          a.tuple(a.literal(42))))).to.equal("(true, 42)");
    });

    it("should evaluate concatenate two arrays", () => {
      expect(evaluate(
        a.concat(
          a.array(a.literal(24)),
          a.array(a.literal(42))))).to.equal("[24, 42]");
    });

    it("should evaluate concatenate two dicts", () => {
      expect(evaluate(
        a.concat(
          a.dict(a.literal("x"), a.literal(1), a.literal("z"), a.literal(2)),
          a.dict(a.literal("x"), a.literal(3), a.literal("y"), a.literal(4)))))
      .to.equal("{x: 3, y: 4, z: 2}");
    });
  });

  describe("index operations", () => {
    it("should evaluate an index on array", () => {
      expect(evaluate(a.index(a.array(a.literal(10), a.literal(42)), a.literal(0)))).to.equal("10");
    });

    it("should evaluate an index on dict", () => {
      const dict = a.dict(a.literal("x"), a.literal(42), a.literal("y"), a.literal(24));
      expect(evaluate(a.index(dict, a.literal("x")))).to.equal("42");
    });

    it("should evaluate an index on tuple", () => {
      expect(evaluate(a.index(a.tuple(a.literal(true), a.literal(42)), a.literal(0)))).to.equal("true");
    });
  });

  describe("length operations", () => {
    it("should evaluate length on array", () => {
      expect(evaluate(a.len(a.array(a.literal(10), a.literal(42))))).to.equal("2");
    });

    it("should evaluate length on dict", () => {
      const dict = a.dict(a.literal("x"), a.literal(42), a.literal("y"), a.literal(24));
      expect(evaluate(a.len(dict))).to.equal("2");
    });

    it("should evaluate length on tuple", () => {
      expect(evaluate(a.len(a.tuple(a.literal(true), a.literal(42))))).to.equal("2");
    });

    it("should evaluate length on string", () => {
      expect(evaluate(a.len(a.literal("abcde")))).to.equal("5");
    });
  });

  describe("cast operations", () => {
    it("should evaluate str of number", () => {
      expect(evaluate(a.str(a.literal(42)))).to.equal("'42'");
    });

    it("should evaluate str of boolean", () => {
      expect(evaluate(a.str(a.literal(true)))).to.equal("'true'");
      expect(evaluate(a.str(a.literal(false)))).to.equal("'false'");
    });

    it("should evaluate bool of number", () => {
      expect(evaluate(a.bool(a.literal(42)))).to.equal("true");
      expect(evaluate(a.bool(a.literal(0)))).to.equal("false");
    });

    it("should evaluate bool of string", () => {
      expect(evaluate(a.bool(a.literal("ab")))).to.equal("true");
      expect(evaluate(a.bool(a.literal("")))).to.equal("false");
    });

    it("should evaluate bool of tuple", () => {
      expect(evaluate(a.bool(a.tuple(a.literal("a"))))).to.equal("true");
      expect(evaluate(a.bool(a.tuple()))).to.equal("false");
    });

    it("should evaluate bool of array", () => {
      expect(evaluate(a.bool(a.array(a.literal("a"))))).to.equal("true");
      expect(evaluate(a.bool(a.array()))).to.equal("false");
    });

    it("should evaluate bool of dict", () => {
      expect(evaluate(a.bool(a.dict(a.literal("a"), a.literal(0))))).to.equal("true");
      expect(evaluate(a.bool(a.dict()))).to.equal("false");
    });
  });

  describe("variables", () => {
    it("should evaluate a variable", () => {
      expect(evaluate(a.variable("x"), def("x", v.Number.of(42)))).to.equal("42");
    });

    it("should evaluate a variable to undefined when variable is unknown", () => {
      expect(evaluate(a.variable("x"))).to.equal("<undefined>");
    });
  });

  describe("control", () => {
    it("should evaluate a conditional", () => {
      expect(evaluate(a.if_then(a.literal(true), a.literal(42), a.literal(0)))).to.equal("42");
      expect(evaluate(a.if_then(a.literal(false), a.literal(42), a.literal(0)))).to.equal("0");
      expect(evaluate(a.if_then(a.literal(false), a.literal(42)))).to.equal("<undefined>");
    });

    it("should evaluate a sequence", () => {
      expect(evaluate(a.seq(a.literal(true), a.literal(42)))).to.equal("42");
      expect(evaluate(a.seq(a.literal(42), a.literal(true)))).to.equal("true");
    });

    it("should evaluate an assignement", () => {
      expect(evaluate(a.assign("x", a.literal(42)))).to.equal("42");
      expect(evaluate(a.seq(a.assign("x", a.literal(42)), a.variable("x")))).to.equal("42");
    });
  });

  describe("functions", () => {
    it("should evaluate inline function", () => {
      expect(evaluate(
        a.call(
          a.func("x",
            a.seq(a.ret(a.literal(42)), a.literal(false))),
          a.literal(2),
        ))).to.equal("42");
    });

    it("should evaluate function by name", () => {
      expect(evaluate(
        a.seq(
          a.assign("add", a.func("x y", a.add(a.variable("x"), a.variable("y")))),
          a.call(a.variable("add"), a.literal(2), a.literal(40))))).to.equal("42");
    });

    it("should evaluate higher order function", () => {
      const addition = a.func(
        "x",
        a.func(
          "y",
          a.add(a.variable("x"), a.variable("y"))));
      const first = a.call(addition, a.literal(2));
      const second = a.call(first, a.literal(40));
      expect(evaluate(second)).to.equal("42");
    });
  });

  describe("lazy evaluation", () => {
    it("should evaluate logical and", () => {
      const collector = [];
      evaluate(a.and(a.literal(false), marker(a.literal(true), collector)));
      expect(collector).to.have.length(0);
    });

    it("should evaluate logical or", () => {
      const collector = [];
      evaluate(a.or(a.literal(true), marker(a.literal(true), collector)));
      expect(collector).to.have.length(0);
    });

    it("should evaluate function call", () => {
      const collector = [];
      evaluate(
        a.call(
          a.func("x", a.literal(42)),
          marker(a.literal(true), collector),
      ));
      expect(collector).to.have.length(0);
    });

  });

  xdescribe("curryfication", () => {
    it("should evaluate function with auto currification", () => {
      expect(evaluate(
        a.call(
          a.call(
            a.func("x y", a.add(a.variable("x"), a.variable("y"))),
            a.literal(2),
          a.literal(40))))).to.equal("42");
    });
  });

  describe("fizzbuzz", () => {
    const fizzbuzz = a.func("v",
      a.seq(
        a.assign("res", a.literal("")),
        a.if_then(
          a.eq(a.mod(a.variable("v"), a.literal(3)), a.literal(0)),
          a.assign("res", a.concat(a.variable("res"), a.literal("Fizz")))),
        a.if_then(
          a.eq(a.mod(a.variable("v"), a.literal(5)), a.literal(0)),
          a.assign("res", a.concat(a.variable("res"), a.literal("Buzz")))),
        a.if_then(
          a.variable("res"),
          a.variable("res"),
          a.str(a.variable("v")))));

    it("should evaluate fizzbuzz", () => {
      expect(evaluate( a.call(fizzbuzz, a.literal(3)))).to.equal("'Fizz'");
      expect(evaluate( a.call(fizzbuzz, a.literal(5)))).to.equal("'Buzz'");
      expect(evaluate( a.call(fizzbuzz, a.literal(15)))).to.equal("'FizzBuzz'");
      expect(evaluate( a.call(fizzbuzz, a.literal(2)))).to.equal("'2'");
    });
  });

});
