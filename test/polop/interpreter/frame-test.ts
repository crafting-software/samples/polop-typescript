import { expect } from "chai";

import { Set } from "immutable";

import { Frame } from "polop/interpreter/frame";
import { value} from "polop/interpreter/lazy";
import * as v from "polop/interpreter/values";


describe("frame", () => {
  it("should return value when symbol is existing", () => {
    const frame = Frame.empty().set("x", value(v.Number.of(42)));
    expect(frame.get("x").get().repr()).to.equal("42");
  });

  it("should return undefined when symbol is unknown", () => {
    const frame = Frame.empty();
    expect(frame.get("x").get().repr()).to.equal("<undefined>");
  });

  it("should build a closure on given symbols", () => {
    const frame = Frame.empty().set("x", value(v.Number.of(42))).closure(Set.of("x"));
    expect(frame.get("x").get().repr()).to.equal("42");
  });
});
