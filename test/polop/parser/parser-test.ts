import { expect } from "chai";

import { Expression } from "polop/ast/expression";
import { Render } from "polop/ast/operations/render";
import { Parser } from "polop/parser/parser";

type Action = (parser: Parser) => Expression;

describe("parser", () => {
  function parse(program: string, action: Action = (p) => p.parse()): string {
    return Render.process(action(new Parser(program)));
  }

  it("should parse integer literal", () => {
    expect(parse("42", (p) => p.number())).to.equal("literal(42)");
  });

  it("should parse boolean literal", () => {
    expect(parse("true", (p) => p.boolean())).to.equal("literal(true)");
    expect(parse("false", (p) => p.boolean())).to.equal("literal(false)");
  });
});
