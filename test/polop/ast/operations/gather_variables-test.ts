import { expect } from "chai";

import { Expression } from "polop/ast/expression";
import * as a from "polop/ast/expressions";
import { GatherVariables } from "polop/ast/operations/gather_variables";


describe("gather variables of expressions", () => {
  function variables_of(expression: Expression): string {
    return GatherVariables.process(expression).sort().join(", ");
  }

  it("should gather nothing from literals", () => {
    expect(variables_of(a.literal(42))).to.equal("");
    expect(variables_of(a.literal(true))).to.equal("");
    expect(variables_of(a.literal(false))).to.equal("");
    expect(variables_of(a.literal(undefined))).to.equal("");
  });

  it("should gather from variable", () => {
    expect(variables_of(a.variable("x"))).to.equal("x");
  });

  it("should gather from sequence", () => {
    expect(variables_of(a.seq(a.variable("x"), a.variable("y"), a.variable("y"))))
      .to.equal("x, y");
  });

});
