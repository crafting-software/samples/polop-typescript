import { expect } from "chai";

import { Expression } from "polop/ast/expression";
import * as a from "polop/ast/expressions";
import { Render } from "polop/ast/operations/render";


describe("render expressions", () => {
  function render(expression: Expression): string {
    return Render.process(expression);
  }

  it("should render a integer literal", () => {
    expect(render(a.literal(42))).to.equal("literal(42)");
  });

  it("should render a boolean literal", () => {
    expect(render(a.literal(true))).to.equal("literal(true)");
    expect(render(a.literal(false))).to.equal("literal(false)");
  });

  it("should render a variable", () => {
    expect(render(a.variable("toto"))).to.equal("variable(toto)");
  });

  it("should render a binary operation", () => {
    expect(render(a.add(a.literal(42), a.literal(0))))
      .to.equal("add(literal(42), literal(0))");

    expect(render(a.combine(a.add, a.literal(1), a.literal(2), a.literal(3))))
      .to.equal("add(add(literal(1), literal(2)), literal(3))");
  });

  it("should render a sequence", () => {
    expect(render(a.seq(a.literal(true), a.literal(42))))
      .to.equal("seq(literal(true), literal(42))");
  });
});
